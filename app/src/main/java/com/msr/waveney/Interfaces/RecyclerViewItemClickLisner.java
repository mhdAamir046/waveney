package com.msr.waveney.Interfaces;

import android.view.View;

public interface RecyclerViewItemClickLisner {
    void onItemClick(View view, int position);
}
