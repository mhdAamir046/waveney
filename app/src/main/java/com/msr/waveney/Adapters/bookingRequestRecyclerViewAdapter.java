package com.msr.waveney.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.msr.waveney.Interfaces.RecyclerViewItemClickLisner;
import com.msr.waveney.R;
import com.msr.waveney.beans.BookingBean;

import java.util.ArrayList;

public class bookingRequestRecyclerViewAdapter extends RecyclerView.Adapter<bookingRequestRecyclerViewAdapter.MyViewHolder> {
    ArrayList<BookingBean> BookingBeanList=new ArrayList<>();
    RecyclerViewItemClickLisner onItemClickListener;
    Context mcontext;
    BookingBean bookingBean;
   public bookingRequestRecyclerViewAdapter(ArrayList<BookingBean> BookingBean, Context mcontext) {
       this.BookingBeanList = BookingBean;
       this.mcontext=mcontext;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView bookingPickUpAddress_tv, bookingDropOffAddress_tv, bookingstatus_tv;
        public MyViewHolder(final View view) {
            super(view);
            bookingPickUpAddress_tv = (TextView) view.findViewById(R.id.bookingPickUpAddress_tv);
            bookingDropOffAddress_tv = (TextView) view.findViewById(R.id.bookingDropOffAddress_tv);
            bookingstatus_tv = (TextView) view.findViewById(R.id.bookingstatus_tv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        if(onItemClickListener!=null)
            onItemClickListener.onItemClick(v,getAdapterPosition());
        }
    }

    public void setItemClickListener(RecyclerViewItemClickLisner clickListener) {
        onItemClickListener = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_request_recyclerview_layout, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
     bookingBean=BookingBeanList.get(position);
     holder.bookingPickUpAddress_tv.setText(bookingBean.getPickup_address());
     holder.bookingDropOffAddress_tv.setText(bookingBean.getDropoff_address());
     holder.bookingstatus_tv.setText(bookingBean.getBooking_status());
    }

    @Override
    public int getItemCount() {
        return BookingBeanList.size();
    }
}
