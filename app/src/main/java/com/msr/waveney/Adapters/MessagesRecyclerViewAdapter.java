package com.msr.waveney.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.msr.waveney.Interfaces.RecyclerViewItemClickLisner;
import com.msr.waveney.R;
import com.msr.waveney.beans.MessageBean;

import java.util.ArrayList;

public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.MyViewHolder> {

    RecyclerViewItemClickLisner onItemClickListener;
    ArrayList<MessageBean>messageBeansList=new ArrayList<>();
    MessageBean messageBean;
    Context mContext;

   public MessagesRecyclerViewAdapter(ArrayList<MessageBean>messageBeansList, Context mContext) {
        this.messageBeansList = messageBeansList;
        this.mContext=mContext;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView preview_msgs_tv, messageSubject_tv, dateTime_tv;
        public MyViewHolder(final View view) {
            super(view);
            preview_msgs_tv = (TextView) view.findViewById(R.id.preview_msgs_tv);
            messageSubject_tv = (TextView) view.findViewById(R.id.messageSubject_tv);
            dateTime_tv = (TextView) view.findViewById(R.id.dateTime_tv);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.messages_recyclerview_layout, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       messageBean=messageBeansList.get(position);
       String dateTime=messageBean.getMsg_date()+" | "+messageBean.getMsg_time();
        holder.preview_msgs_tv.setText(messageBean.getBody());
        holder.messageSubject_tv.setText(messageBean.getSubject());
        holder.dateTime_tv.setText(dateTime);
    }

    @Override
    public int getItemCount() {
        return messageBeansList.size();
    }
}
