package com.msr.waveney.beans;

import java.io.Serializable;

public class BookingDetailSBean implements Serializable {
    String FullName;
    String contatNumber;
    String payment;
    String pickUpAddress;
    String dropOffAddress;
    String pickUpDate;
    String pickUpTime;
    String fareTotal;
    String bookingStatus;

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getContatNumber() {
        return contatNumber;
    }

    public void setContatNumber(String contatNumber) {
        this.contatNumber = contatNumber;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDropOffAddress() {
        return dropOffAddress;
    }

    public void setDropOffAddress(String dropOffAddress) {
        this.dropOffAddress = dropOffAddress;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getPickUpTime() {
        return pickUpTime;
    }

    public void setPickUpTime(String pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public String getFareTotal() {
        return fareTotal;
    }

    public void setFareTotal(String fareTotal) {
        this.fareTotal = fareTotal;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }
}
