package com.msr.waveney.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.gson.Gson;
import com.msr.waveney.R;
import com.msr.waveney.Services.LoginJobService;
import com.msr.waveney.beans.UserBean;
import com.msr.waveney.controller.AppController;
import com.msr.waveney.controller.ConfigURL;
import com.msr.waveney.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class LoginActivity extends AppCompatActivity  {
    EditText etUserName, etPassword;
    Button btnLogin;
    private ProgressDialog pDialog;
    ImageView car;
    private final String TAG=LoginActivity.class.getSimpleName();
    boolean internetState=true;
    private SessionManager session;
    FirebaseJobDispatcher dispatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new SessionManager();
        pDialog = new ProgressDialog(this);
        etUserName = (EditText)findViewById(R.id.etUserName);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btnLogin=(Button)findViewById(R.id.btnLogin);
        car=(ImageView)findViewById(R.id.ivCar);
        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));

        if(!checkInternetConnectionAvailable()){
            Toast.makeText(this, "No Internet Available", Toast.LENGTH_SHORT).show();
            internetState=false;
        }
        requestPermissions();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(LoginActivity.this, ActivityVehicleCheck.class);
//                startActivity(intent);
//                finish();
                if (etUserName.getText().toString().trim().equalsIgnoreCase("")) {
                    etUserName.setError("This field can not be blank");
                }else if(etPassword.getText().toString().trim().equalsIgnoreCase("")){
                    etPassword.setError("This field can not be blank");
                }else if(checkInternetConnectionAvailable()) {
                    checkLogin(etUserName.getText().toString(), etPassword.getText().toString());
                    Intent intent=new Intent(LoginActivity.this,ActivityVehicleCheck.class);
                    startActivity(intent);
                    finish();
                }
                else
                    Toast.makeText(LoginActivity.this, "No Internet Available", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestPermissions() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
           /* if(checkSelfPermission(Manifest.permission.READ_PHONE_STATE)!= PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},2);
            }*/
            if(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
            if(checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)!=PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.ACCESS_NETWORK_STATE},1);
            }
        }
    }

    private void checkLogin(final String username, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";
//        pDialog.setMessage("Logging in ...");
//        showDialog();
        Bundle myExtrasBundle = new Bundle();
        myExtrasBundle.putString("username", "username");
        myExtrasBundle.putString("password", "password");

        Job myJob = dispatcher.newJobBuilder()
                // the JobService that will be called
                .setService(LoginJobService.class)
                // uniquely identifies the job
                .setTag("LoginJobService-tag")
                // one-off job
                .setRecurring(false)
                // persist past a device reboot
                .setLifetime(Lifetime.FOREVER)
                // start Now
                .setTrigger(Trigger.NOW)
                // don't overwrite an existing job with the same tag
                .setReplaceCurrent(false)
                // retry with exponential backoff
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                // constraints that need to be satisfied for the job to run
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setExtras(myExtrasBundle)
                .build();

        int result=dispatcher.schedule(myJob);
        if (result == FirebaseJobDispatcher.SCHEDULE_RESULT_SUCCESS) {
            Log.d(TAG,"JobScheduledSuccessfully");
        } else {
            Log.d(TAG,"JobScheduledFailed");
        }

//        StringRequest strReq = new StringRequest(Request.Method.POST,
//                ConfigURL.URL_LOGIN, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, "Login Response: " + response.toString());
//                hideDialog();
//
//                try {
//                    JSONObject jObj = new JSONObject(response);
//                    Toast.makeText(LoginActivity.this, jObj.toString(), Toast.LENGTH_SHORT).show();
//                    boolean error = jObj.getBoolean("error");
//                    if(!error){
//                        UserBean userBean = new Gson().fromJson(jObj.getString("user"), UserBean.class);
//                        new SessionManager().setUser(userBean);
//                    }
//
//
//                } catch (JSONException e) {
//                    Log.e(TAG,e.getMessage());
//                    Toast.makeText(LoginActivity.this, "Something went Wrong (:", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Login Error: " + error.getMessage());
//                Toast.makeText(LoginActivity.this, "Something went Wrong (:", Toast.LENGTH_SHORT).show();
//                hideDialog();
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting parameters to login url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("username", username);
//                params.put("password", password);
//
//                return params;
//            }
//
//        };
//
//        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        dist = dist * 1609.344;
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private boolean checkInternetConnectionAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();

    }
}
