package com.msr.waveney.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.msr.waveney.R;
import com.msr.waveney.beans.BookingDetailSBean;

import java.util.ArrayList;
import java.util.List;

public class BookingDetailsActivtiy extends Activity implements AdapterView.OnItemSelectedListener {
    Button backBtn;
    TextView contact_tv;
    TextView fullName_tv;
    TextView paymentMethod_tv;
    TextView pickUpAddress_tv;
    TextView dropOffAddress_tv;
    TextView pickUpDate_tv;
    TextView pickTime_tv;
    TextView fareTotal_tv;
    Spinner spinnerBookingStatus;
    BookingDetailSBean bookingDetailSBean;
    String bookingStatus="";

    @Override
    protected void onCreate(Bundle savedInstanceState)   {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_booking_detail);
        backBtn=findViewById(R.id.back_btn);
        contact_tv=findViewById(R.id.contact_tv);
        fullName_tv=findViewById(R.id.fullName_tv);
        paymentMethod_tv=findViewById(R.id.paymentMethod_tv);
        pickUpAddress_tv=findViewById(R.id.pickUpAddress_tv);
        dropOffAddress_tv=findViewById(R.id.dropOffAddress_tv);
        pickUpDate_tv=findViewById(R.id.pickUpDate_tv);
        pickTime_tv=findViewById(R.id.pickTime_tv);
        fareTotal_tv=findViewById(R.id.fareTotal_tv);
        spinnerBookingStatus=findViewById(R.id.bookingStatusSpinner);
        spinnerBookingStatus.setOnItemSelectedListener(this);
        Intent intent=getIntent();
        bookingDetailSBean= (BookingDetailSBean) intent.getSerializableExtra("bookingDetailsBean");
        fullName_tv.setText(bookingDetailSBean.getFullName());
        contact_tv.setText(bookingDetailSBean.getContatNumber());
        paymentMethod_tv.setText(bookingDetailSBean.getPayment());
        pickUpAddress_tv.setText(bookingDetailSBean.getPickUpAddress());
        dropOffAddress_tv.setText(bookingDetailSBean.getDropOffAddress());
        pickUpDate_tv.setText(bookingDetailSBean.getPickUpDate());
        pickTime_tv.setText(bookingDetailSBean.getPickUpTime());
        fareTotal_tv.setText(bookingDetailSBean.getFareTotal());
        //spinnerBookingStatus.setText(bookingDetailSBean.getBookingStatus());
        List<String> categories = new ArrayList<String>();
        categories.add("New Booking");
        categories.add("On Way to pick up");
        categories.add("On way to drop off");
        categories.add("Cancelled");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
        spinnerBookingStatus.setAdapter(dataAdapter);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        contact_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactNumber= (String) contact_tv.getText();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+contactNumber));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        bookingStatus=parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "Nothing Selected", Toast.LENGTH_SHORT).show();
    }
}
