package com.msr.waveney.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.msr.waveney.Adapters.MessagesRecyclerViewAdapter;
import com.msr.waveney.Adapters.bookingRequestRecyclerViewAdapter;
import com.msr.waveney.Interfaces.RecyclerViewItemClickLisner;
import com.msr.waveney.R;
import com.msr.waveney.beans.BookingBean;
import com.msr.waveney.beans.BookingDetailSBean;
import com.msr.waveney.beans.MessageBean;
import com.msr.waveney.controller.AppController;
import com.msr.waveney.controller.ConfigURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookingRequestsActivty extends Activity implements RecyclerViewItemClickLisner {
    RecyclerView bookingsRecyclerView;
    bookingRequestRecyclerViewAdapter mAdapter;
    ArrayList<String> pickingAddressList=new ArrayList<>();
    ArrayList<String> DropAddressList=new ArrayList<>();
    ArrayList<String> statusList=new ArrayList<>();
    ArrayList<BookingBean> BookingBeanList=new ArrayList<>();
    Button backBtn;
    private ProgressDialog pDialog;
    private final String TAG=BookingDetailsActivtiy.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_request);
        bookingsRecyclerView=findViewById(R.id.bookingRequestList);
        backBtn=findViewById(R.id.backRequest_btn);
        pDialog = new ProgressDialog(this);

        mAdapter=new bookingRequestRecyclerViewAdapter(BookingBeanList,BookingRequestsActivty.this);
        mAdapter.setItemClickListener(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        bookingsRecyclerView.setLayoutManager(mLayoutManager);
        bookingsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        bookingsRecyclerView.setAdapter(mAdapter);
        getBookings();
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BookingRequestsActivty.this,MainScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getBookings() {
        String tag_string_req = "req_login";
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        bookingsRecyclerView.setLayoutManager(mLayoutManager);
        bookingsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        pDialog.setMessage("Getting All Messages ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConfigURL.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    Toast.makeText(BookingRequestsActivty.this, jObj.toString(), Toast.LENGTH_SHORT).show();
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                         BookingBeanList = new Gson().fromJson(jObj.getString("booking"), new TypeToken<List<BookingBean>>(){}.getType());
                        Log.d("bookingBeans",""+BookingBeanList.get(0).getDropoff_address());
                    }
                    mAdapter=new bookingRequestRecyclerViewAdapter(BookingBeanList,BookingRequestsActivty.this);
                    mAdapter.setItemClickListener(BookingRequestsActivty.this);
                    bookingsRecyclerView.setAdapter(mAdapter);
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("getBooking","1");
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onItemClick(View view, int position) {

        BookingBean bookingBean= BookingBeanList.get(position);
        Log.d(TAG,""+bookingBean);
        BookingDetailSBean bookingDetailSBean=new BookingDetailSBean();
        bookingDetailSBean.setContatNumber(bookingBean.getCustomer_number());
        bookingDetailSBean.setPickUpAddress(bookingBean.getPickup_address());
        bookingDetailSBean.setDropOffAddress(bookingBean.getDropoff_address());
        bookingDetailSBean.setPickUpDate(bookingBean.getPickup_date());
        bookingDetailSBean.setPickUpTime(bookingBean.getPickup_time());
        bookingDetailSBean.setFareTotal(bookingBean.getFare_total());
        bookingDetailSBean.setBookingStatus(bookingBean.getBooking_status());
        Intent intent=new Intent(BookingRequestsActivty.this,BookingDetailsActivtiy.class);
        intent.putExtra("bookingDetailsBean",bookingDetailSBean);
        startActivity(intent);
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
