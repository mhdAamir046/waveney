package com.msr.waveney.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.msr.waveney.R;

public class ComposeMessageActivtiy extends Activity {
    Button sendMessageBtn,backBtn;
    EditText subject_et;
    EditText msgBody_et;
    String subject,msgBody;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_compose_message);
        sendMessageBtn=findViewById(R.id.sendBtn);
        backBtn=findViewById(R.id.backBtn);
        subject_et=findViewById(R.id.messageSubject_et);
        msgBody_et=findViewById(R.id.messageBody_et);

        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subject=subject_et.getText().toString();
                msgBody=msgBody_et.getText().toString();
                Toast.makeText(ComposeMessageActivtiy.this, "Sent Successfully", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
