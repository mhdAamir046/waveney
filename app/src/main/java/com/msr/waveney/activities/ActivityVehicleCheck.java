package com.msr.waveney.activities;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.msr.waveney.Adapters.bookingRequestRecyclerViewAdapter;
import com.msr.waveney.R;
import com.msr.waveney.beans.BookingBean;
import com.msr.waveney.controller.AppController;
import com.msr.waveney.controller.ConfigURL;
import com.msr.waveney.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityVehicleCheck extends Activity implements AdapterView.OnItemSelectedListener {
    Button completeCheckBtn;
    Button photoButton;
    TextView todaysTime_et;
    TextView todaysDate_et;
    EditText vehicleRegistration_et;
    EditText vehicleMileage_et;
    EditText defect_et;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Bitmap bitmap;
    String vehicleFuel="";
    Spinner spinner;
    CheckBox lightCheckBox;
    CheckBox tyresCheckBox;
    CheckBox fluidLevelsCheckBox;
    CheckBox windowWisperCheckBox;
    CheckBox seatBeltsCheckBox;
    CheckBox fireExtinguisherCheckBox;
    String lights,tyres,fluidLevel,windowWisper,seatBelts,fireExtinguisher;
    String vehicleRegistration,vehicleMileage,vehicleDefects;
    String time,date;
    private ProgressDialog pDialog;
    String TAG="ActivtiyVehicleCheck";
    String tag_string_req="imageUplaod";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehiclecheck);
        completeCheckBtn=findViewById(R.id.completeCheckBtn);
        photoButton=findViewById(R.id.photoButton);
        todaysTime_et=findViewById(R.id.todaysTime_et);
        todaysDate_et=findViewById(R.id.todaysDate_et);
        spinner=findViewById(R.id.spinner);
        lightCheckBox=findViewById(R.id.lightCheckBox);
        tyresCheckBox=findViewById(R.id.tyresCheckBox);
        fluidLevelsCheckBox=findViewById(R.id.fluidLevelsCheckBox);
        windowWisperCheckBox=findViewById(R.id.windowWisperCheckBox);
        seatBeltsCheckBox=findViewById(R.id.seatBeltsCheckBox);
        fireExtinguisherCheckBox=findViewById(R.id.fireExtinguisherCheckBox);
        defect_et=findViewById(R.id.defect_et);
        vehicleRegistration_et=findViewById(R.id.vehicleRegistration_et);
        vehicleMileage_et=findViewById(R.id.vehicleMileage_et);
        pDialog = new ProgressDialog(this);

        todaysDate_et.setText(getCurrentDate());
        todaysTime_et.setText(getCurrentTimeUsingDate());
        spinner.setOnItemSelectedListener(this);
        requestPermissions();

        List<String> categories = new ArrayList<String>();
        categories.add("Empty");
        categories.add("Quarter");
        categories.add("Half");
        categories.add("Three Quarter");
        categories.add("Full");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
        spinner.setAdapter(dataAdapter);

        completeCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lights=(lightCheckBox.isChecked())?"Yes":"No";
                tyres=(tyresCheckBox.isChecked())?"Yes":"No";
                fluidLevel=(fluidLevelsCheckBox.isChecked())?"Yes":"No";
                windowWisper=(windowWisperCheckBox.isChecked())?"Yes":"No";
                seatBelts=(seatBeltsCheckBox.isChecked())?"Yes":"No";
                fireExtinguisher=(fireExtinguisherCheckBox.isChecked())?"Yes":"No";
                vehicleRegistration=vehicleRegistration_et.getText().toString();
                vehicleMileage=vehicleMileage_et.getText().toString();
                vehicleDefects=defect_et.getText().toString();
                Intent intent=new Intent(ActivityVehicleCheck.this,MainScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });

        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
        todaysDate_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialogue();
            }
        });

        todaysTime_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialogue();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this, "rCode="+requestCode+"rsltCode="+resultCode, Toast.LENGTH_SHORT).show();
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == -1 && data != null) {
         //   Uri uri = data.getData();
            try {
                // Adding captured image in bitmap.
                bitmap = (Bitmap) data.getExtras().get("data");
                Toast.makeText(this, ""+bitmap, Toast.LENGTH_SHORT).show();
                uploadImage(bitmap);
                // adding captured image in imageview.
               // ImageViewHolder.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage(Bitmap bitmap) {
        Toast.makeText(this, "UploadImage", Toast.LENGTH_SHORT).show();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        final byte[] byteArray = stream.toByteArray();
//        final ByteArrayOutputStream byteArrayOutputStreamObject ;
//        byteArrayOutputStreamObject = new ByteArrayOutputStream();
//        // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
//
//        final byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConfigURL.URL_FILE_UPLOAD, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Toast.makeText(ActivityVehicleCheck.this, ""+response, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Response: " + response.toString());
                hideDialog();


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityVehicleCheck.this, ""+error, Toast.LENGTH_LONG).show();
                Log.e(TAG, "Login Error: " + error.getMessage());
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("image", String.valueOf(byteArray));
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
     //   final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
    }

    private String getCurrentTimeUsingDate() {
        Date date = new Date();
        String strDateFormat = "hh:mm a";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        System.out.println("Current time of the day using Date - 12 hour format: " + formattedDate);
        return formattedDate;

    }

    private String getCurrentDate(){
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        String dateToStr = format.format(today);
        return dateToStr;
    }

    private void requestPermissions() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){

            if(checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.CAMERA},1);
            }

        }
    }

    private void showDateDialogue() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        date=dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
                        todaysDate_et.setText(date);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showTimeDialogue() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String am_pm="";
                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);
                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";
                        String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":Integer.toString( datetime.get(Calendar.HOUR) );
                        String strinMntToShow=(minute<10) ?"0"+minute : Integer.toString(minute);
                        time=strHrsToShow+":"+strinMntToShow+" "+am_pm;
                        todaysTime_et.setText(time);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        vehicleFuel=parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
