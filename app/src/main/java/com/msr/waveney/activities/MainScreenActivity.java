package com.msr.waveney.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.msr.waveney.R;

public class MainScreenActivity extends Activity {
    Button messages_btn,bookings_btn,logOffBtn,callOfficeBtn;
    boolean doubleBackToExitPressedOnce=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        messages_btn=findViewById(R.id.messages_btn);
        bookings_btn=findViewById(R.id.bookings_btn);
        logOffBtn=findViewById(R.id.logOff_btn);
        callOfficeBtn=findViewById(R.id.callOffice_btn);

        messages_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainScreenActivity.this,MessagesActivity.class);
                startActivity(intent);
                finish();
            }
        });

        bookings_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainScreenActivity.this,BookingRequestsActivty.class);
                startActivity(intent);
                finish();
            }
        });

        logOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainScreenActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        callOfficeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0123456789"));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
