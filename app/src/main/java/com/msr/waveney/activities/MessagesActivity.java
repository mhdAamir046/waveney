package com.msr.waveney.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.msr.waveney.Adapters.MessagesRecyclerViewAdapter;
import com.msr.waveney.Interfaces.RecyclerViewItemClickLisner;
import com.msr.waveney.R;
import com.msr.waveney.beans.MessageBean;
import com.msr.waveney.beans.UserBean;
import com.msr.waveney.controller.AppController;
import com.msr.waveney.controller.ConfigURL;
import com.msr.waveney.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesActivity extends Activity {
    RecyclerView messagesRecyclerView;
    MessagesRecyclerViewAdapter mAdapter;
    ArrayList<String> messageslist = new ArrayList<>();
    Button back, conmposeMessageBtn;
    private ProgressDialog pDialog;
    private final String TAG=MessagesActivity.class.getSimpleName();
    ArrayList<MessageBean>messageBeansList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        back = findViewById(R.id.backBtn);
        pDialog = new ProgressDialog(this);
        conmposeMessageBtn = findViewById(R.id.composeBtn);
        messagesRecyclerView = findViewById(R.id.messagesRecyclerView);
        getMessages();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessagesActivity.this, MainScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });

        conmposeMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessagesActivity.this, ComposeMessageActivtiy.class);
                startActivity(intent);
            }
        });
    }

    private void getMessages() {
            // Tag used to cancel the request
             String tag_string_req = "req_login";
             RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
             messagesRecyclerView.setLayoutManager(mLayoutManager);
             messagesRecyclerView.setItemAnimator(new DefaultItemAnimator());
             pDialog.setMessage("Getting All Messages ...");
             showDialog();

             StringRequest strReq = new StringRequest(Request.Method.POST,
                    ConfigURL.URL_LOGIN, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Login Response: " + response.toString());
                    hideDialog();

                    try {
                        JSONObject jObj = new JSONObject(response);
                        Toast.makeText(MessagesActivity.this, jObj.toString(), Toast.LENGTH_SHORT).show();
                        boolean error = jObj.getBoolean("error");
                        if (!error) {
                            messageBeansList = new Gson().fromJson(jObj.getString("messages"), new TypeToken<List<MessageBean>>(){}.getType());
                            mAdapter = new MessagesRecyclerViewAdapter(messageBeansList,MessagesActivity.this);
                            messagesRecyclerView.setAdapter(mAdapter);
                            Log.d("MessageBeans",""+messageBeansList);
                        }


                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Login Error: " + error.getMessage());

                    hideDialog();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("getMessages","1");

                    return params;
                }

            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
