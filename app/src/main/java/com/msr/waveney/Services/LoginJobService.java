package com.msr.waveney.Services;

import android.app.ProgressDialog;
import android.app.job.JobParameters;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.firebase.jobdispatcher.JobService;
import com.google.gson.Gson;
import com.msr.waveney.activities.LoginActivity;
import com.msr.waveney.beans.UserBean;
import com.msr.waveney.controller.AppController;
import com.msr.waveney.controller.ConfigURL;
import com.msr.waveney.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginJobService extends JobService {
    BackgroundTask backgroundTask;
    static String TAG="LoginJobService";
    static boolean signInSuccess=true;
    static String username;
    static String password;

    @Override
    public boolean onStartJob(@NonNull final com.firebase.jobdispatcher.JobParameters job) {
        Log.d(TAG,"onStartJob");
        backgroundTask = new BackgroundTask() {
            @Override
            protected void onPreExecute() {
                Bundle bundle =job.getExtras();
                username=bundle.getString("username");
                password=bundle.getString("password");
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                if(s.equals("success"))
                    jobFinished(job,false);
                else
                    jobFinished(job,true);
                Toast.makeText(getApplicationContext(),"Login :"+s,Toast.LENGTH_LONG).show();
            }
        };

        backgroundTask.execute();
        return true;
    }

    @Override
    public boolean onStopJob(@NonNull com.firebase.jobdispatcher.JobParameters job) {
        Log.d(TAG,"onStopJob");
        return true;
    }

    public static class BackgroundTask extends AsyncTask<Void,Void,String>
    {

        @Override
        protected String doInBackground(Void... voids) {
            String tag_string_req = "req_login";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    ConfigURL.URL_LOGIN, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Login Response: " + response.toString());
                    signInSuccess=true;

                    try {
                        JSONObject jObj = new JSONObject(response);
                        Log.d(TAG, "JsonObject: " + jObj);
                        boolean error = jObj.getBoolean("error");
                        if(!error){
                            UserBean userBean = new Gson().fromJson(jObj.getString("user"), UserBean.class);
                            new SessionManager().setUser(userBean);
                        }


                    } catch (JSONException e) {
                        Log.e(TAG,e.getMessage());
                        signInSuccess=false;
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Login Error: " + error.getMessage());
                    signInSuccess=false;
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", username);
                    params.put("password", password);

                    return params;
                }

            };
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
            // Adding request to request queue
            if (signInSuccess)
                return "success";
            else
                return "fail";
        }
    }

}
