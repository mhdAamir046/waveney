package com.msr.waveney.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.msr.waveney.beans.UserBean;
import com.msr.waveney.controller.AppController;


public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "TrackingApp";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public SessionManager() {
        this._context = AppController.getInstance();
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }


    public void setUser(UserBean user){
        String JSONLocation = new Gson().toJson(user);
        Log.e(TAG,JSONLocation);
        editor.putString("user", JSONLocation);
        // commit changes
        editor.commit();
    }
    public UserBean getUser(){

        String userStr=pref.getString("user",null);
        UserBean user=new Gson().fromJson(userStr,UserBean.class);
        return  user;

    }



}
